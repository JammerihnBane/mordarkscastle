﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {

    public Text keyText;
    public int numKeys = 0;

    Player playerComponent = null;

    public static UI instance = null;

    private void Awake()
    {
        if( instance == null )
            instance = this;
        else
        {
            Debug.LogError("Two UIs, this is weird");
        }

        SetKeyText(numKeys);
    }

	// Update is called once per frame
	void Update () {
        //Bad to do this each frame but I'm just hacking this together for now.
        if( playerComponent )
        {
            //if(playerComponent.NumberOf("SmallKey") != numKeys)
            //{
                SetKeyText(playerComponent.NumberOf("SmallKey"));
            //}
        }
        else
        {
            playerComponent = Player.player.GetComponent<Player>();
        }
        
    }

    //Sets the on-screen key counter.
    public void SetKeyText( int _numKeys )
    {
        keyText.text = "x " + _numKeys.ToString();
    }
}
