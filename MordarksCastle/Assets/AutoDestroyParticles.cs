﻿using UnityEngine;
using System.Collections;

public class AutoDestroyParticles : MonoBehaviour {

    ParticleSystem ps = null;

	// Use this for initialization
	void Start () {
	    ps = GetComponent< ParticleSystem > ();
        if (ps == null)
            Debug.LogError("Cannot auto-destroy this, it doesn't have a particle system.");
	}
	
	// Update is called once per frame
	void Update () {
	    if( ps.IsAlive() == false )
        {
            GameObject.Destroy(gameObject);
        }
	}
}
