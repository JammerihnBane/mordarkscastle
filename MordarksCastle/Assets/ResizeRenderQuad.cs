﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizeRenderQuad : MonoBehaviour {

    public PixelPerfectOrthographic pixelCam;

    MeshRenderer rend = null;


	// Use this for initialization
	void Start () {
        rend = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        float width = rend.material.mainTexture.width;
        float height = rend.material.mainTexture.height;

        transform.localScale = new Vector3(width / pixelCam.pixelsPerUnit, height / pixelCam.pixelsPerUnit, 1f);
	}
}
