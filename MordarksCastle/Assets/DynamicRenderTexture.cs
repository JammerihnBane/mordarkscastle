﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(PixelPerfectOrthographic))]
public class DynamicRenderTexture : MonoBehaviour {

    private Camera thisCam = null;

    public MeshRenderer renderQuad;

	// Use this for initialization
	void Start () {
        thisCam = GetComponent<Camera>();
        int zoomLevel = (int)(GetComponent<PixelPerfectOrthographic>().zoomLevel);
        RenderTexture rt = new RenderTexture(Screen.width / zoomLevel, Screen.height / zoomLevel, 16);
        rt.wrapMode         = TextureWrapMode.Clamp;
        rt.filterMode       = FilterMode.Point;
        rt.antiAliasing     = 1;

        thisCam.targetTexture = rt;

        renderQuad.material.mainTexture = rt;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
