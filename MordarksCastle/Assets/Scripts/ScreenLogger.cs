﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScreenLogger : MonoBehaviour {

    public static ScreenLogger instance = null;
    public bool log = false;
    public int capacity = 10;

    private List<string> LogStrings = null;

	// Use this for initialization
	void Start () {
        if( instance == null )
        {
            instance = this;
            LogStrings = new List<string>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
        
	}
	
	// Update is called once per frame
	void OnGUI ()
    {
        if(log)
        {
            for (int i = LogStrings.Count - 1; i >= 0; --i)
            {
                GUI.contentColor = Color.red;
                GUILayout.Label(LogStrings[i]);
            }
        }
	}

    //Logs stuff to the screen cos Unity is fucking useless.
    //If stuff exceeds capacity then it removes the oldest string.
    public static void Log(string _text)
    {
        instance.LogStrings.Add(_text);
        if(instance.LogStrings.Count > instance.capacity )
        {
            instance.LogStrings.RemoveAt(0);
        }
    }
}
