﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

    public GameObject playerPrefab;

	// Use this for initialization
	void Start () {
	    if( Player.player == null )
        {
            GameObject player = GameObject.Instantiate(playerPrefab);
            Player.player = player;
            player.transform.position = transform.position;
        }
	}
}
