﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    PixelPerfectOrthographic ppo = null;
    Vector3 targetPos = Vector3.zero;
    Vector3 oldPos = Vector3.zero;
    float lerpValue = 0f;
    Vector3 startPos = Vector3.zero;
    public float lerpSpeed;
    float distanceModifier;

    private float vertExtent = 0.0f; //Half of the vertical size that the camera can see.
    private float horizExtent = 0.0f; //Half of the horizontal size that the camera can see.

    private Rect cameraConstraintRect;

    // Use this for initialization
    void Start()
    {
        ppo = GetComponent<PixelPerfectOrthographic>();
        CalculateCameraBounds();
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.player)
        {
            Room room = WorldManager.worldManager.GetRoom(Player.player.transform.position);
            if (room != null)
            {
                CalculateTargetPos(room);
                targetPos.z = transform.position.z;
                if (oldPos != targetPos)
                {
                    startPos = transform.position;
                    distanceModifier = (targetPos - startPos).magnitude;
                    lerpValue = 0f;
                }
                else
                {
                    if (lerpValue < 1f)
                        lerpValue += Time.deltaTime * lerpSpeed * 1/distanceModifier;
                }

                Debug.Log(lerpValue);

                Vector3 tPos = ((targetPos - startPos) * lerpValue) + startPos;
                transform.position = tPos;
                ppo.RoundPos();

                oldPos = targetPos;
            }
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    void CalculateCameraBounds()
    {
        vertExtent = Camera.main.orthographicSize;
        horizExtent = vertExtent * (float)(Screen.width) / (float)(Screen.height);
    }

    void CalculateTargetPos( Room _room )
    {
        CalculateCameraBounds();

        targetPos = Player.player.transform.position;
        targetPos.z = transform.position.z;

        CalculateConstrainRect( _room );

        ClampCamPos();
    }

    private void ClampCamPos()
    {
        targetPos.x = Mathf.Clamp(targetPos.x, cameraConstraintRect.xMin, cameraConstraintRect.xMax);
        targetPos.y = Mathf.Clamp(targetPos.y, cameraConstraintRect.yMin, cameraConstraintRect.yMax);
        targetPos = ppo.RoundPos(targetPos);

        //Vector3 camPos = Camera.main.transform.position;
        //camPos.x = Mathf.Clamp(camPos.x, cameraConstraintRect.xMin, cameraConstraintRect.xMax);
        //camPos.y = Mathf.Clamp(camPos.y, cameraConstraintRect.yMin, cameraConstraintRect.yMax);
        //Camera.main.transform.position = camPos;
    }

    private void CalculateConstrainRect( Room _room )
    {
        Rect roomRect = _room.GetRoomRect();
        if (roomRect.width * 0.5f < horizExtent)
        {
            cameraConstraintRect.xMin = roomRect.center.x;
            cameraConstraintRect.xMax = roomRect.center.x;
        }
        else
        {
            cameraConstraintRect.xMin = roomRect.xMin + horizExtent;
            cameraConstraintRect.xMax = roomRect.xMax - horizExtent;
        }

        if (_room.GetRoomRect().height * 0.5f < vertExtent)
        {
            cameraConstraintRect.yMin = roomRect.center.y;
            cameraConstraintRect.yMax = roomRect.center.y;
        }
        else
        {
            cameraConstraintRect.yMin = roomRect.yMin + vertExtent;
            cameraConstraintRect.yMax = roomRect.yMax - vertExtent;
        }
    }
}

