﻿using UnityEngine;
using System.Collections;

public class SaveData : MonoBehaviour {

    //This class is used to store data that changes throughout the game.
    //Is this key collected, has this door been opened, where is this crate etc.
    public RawSaveData rawData;
    private void CreateSaveData()
    {
        if (rawData == null)
            rawData = new RawSaveData();
    }

    //Updates the save data in the SaveGameManager(fast) but
    //does not save the game to disk(slow).
    public void SaveLocally()
    {
        SaveGameManager.instance.SaveDataLocally(rawData);
    }

    public void SetObjectID( string _roomName, int _id, bool _loadImmediately = true )
    {
        if(SaveGameManager.instance != null )
        {
            CreateSaveData(); //Takes into account whether the save data exists.

            rawData.id = _roomName + "." + _id;
            if (_loadImmediately == true && Application.isPlaying == true)
            {
                RawSaveData tempData = SaveGameManager.instance.LoadDataLocally(rawData.id);
                if (tempData != null)
                {
                    rawData = tempData; //Set our data to the loaded data, if data was loaded.
                    if (rawData.GetPos() != Vector3.zero) //Set position if there's a valid position.
                    {
                        transform.position = rawData.GetPos();
                        PushActivator pa = GetComponent<PushActivator>();
                        if (pa)
                        {
                            pa.MoveFinished(); //Activate anything that this statue happens to be on top of.
                        }
                    }

                    ExtraData ed = GetComponent<ExtraData>();
                    if (ed != null)
                    {
                        ed.rawData.state = rawData.state;
                    }
                }

            }
        }
    }
}

[System.Serializable]
public class RawSaveData
{
    public bool state;
    private float x;
    private float y;
    private float z;
    public string id = "";

    public void SetPos( Vector3 _vec )
    {
        x = _vec.x;
        y = _vec.y;
        z = _vec.z;
    }

    public Vector3 GetPos()
    {
        return new Vector3(x, y, z);
    }
}