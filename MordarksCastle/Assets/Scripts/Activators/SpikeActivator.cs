﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpikeActivator : MonoBehaviour {

    public bool up = true; //Gonna make this setable from extra data.

    private SolidActiveObject sao = null;
    //private Room myRoom = null;

    private bool blocked = false;

	// Use this for initialization
	void Start () {
        //GetComponent<Animator>().Stop();

        Activator act = GetComponent<Activator>();
        if( act != null )
        {
            act.AddActivator( Activate );
            act.AddDeactivator( DeActivate );
        }
        sao = GetComponent<SolidActiveObject>();

        ExtraData ed = GetComponent<ExtraData>();
        if( ed )
        {
            up = !ed.rawData.state;
            sao.solid = up;
            GetComponent<Animator>().SetBool(Animator.StringToHash("up"), up);
        }

        //myRoom = WorldManager.worldManager.GetRoom(transform.position);
    }

    //This will animate the spikes if they are in the right state.
    void Update()
    {
        //Keep the spikes down if they are blocked.
        if (blocked == true)
        {
            GetComponent<Animator>().SetBool(Animator.StringToHash("up"), false);
        }
        else
        {
            GetComponent<Animator>().SetBool(Animator.StringToHash("up"), up);
        }
    }
    
    //Switch the state of the spikes.
    void Activate( GameObject _activator )
    {
        //Players cannot activate spikes.
        if ( _activator.GetComponent<Player>() == null && _activator.GetComponent<PushActivator>() == null)
        {
            //Debug.Log("Spikes Activated.");
            up = !up;
            sao.solid = up;
            //GetComponent<Animator>().SetBool(Animator.StringToHash("up"), up);
        }
    }

    void DeActivate(GameObject _activator)
    {
        if (_activator.GetComponent<Player>() == null && _activator.GetComponent<PushActivator>() == null)
        {
            //Debug.Log("Spikes Activated.");
            up = !up;
            sao.solid = up;
            //GetComponent<Animator>().SetBool(Animator.StringToHash("up"), up);
        }
    }

    public static void BlockSpikesAt(Vector3 _position)
    {
        SetBlockedAt(_position, true);
    }

    public static void UnblockSpikesAt(Vector3 _position)
    {
        SetBlockedAt(_position, false);
    }

    public static void SetBlockedAt( Vector3 _position, bool _state = true)
    {
        List<GameObject> objects = WorldManager.worldManager.GetObjects(_position);
        foreach (GameObject obj in objects)
        {
            SpikeActivator act = obj.GetComponent<SpikeActivator>();
            if (act)
                act.blocked = _state;
        }
    }
}
