﻿using UnityEngine;
using System.Collections;

public class SecretWallActivator : MonoBehaviour {

    Activator           act = null;
    SolidActiveObject   sao = null;
    ExtraData ed = null;
    public GameObject revealEffect;

	// Use this for initialization
	void Start () {
        act = GetComponent<Activator>();
        act.AddActivator(onActivate);

        ed = GetComponent<ExtraData>();
        sao = GetComponent<SolidActiveObject>();

        if (ed.rawData.state == true)
            DestroyDoor(false);
	}


    void onActivate(GameObject _activator)
    {
        Debug.Log("Activating secret door...");
        if (sao.solid == true)
        {
            DestroyDoor(true);
        }
        else
        {
            Player player = _activator.GetComponent<Player>();
            if (player != null)
            {
                if (ed.rawData.connections.Length == 0)
                    Debug.LogError("This teleporter doesn't contain a connection. abandoning teleport.");
                else
                {
                    //Teleport.
                    WorldManager.worldManager.TeleportPlayer(ed.rawData.connections[0]);
                }
            }
        }
    }

    //Blows up the secret door.
    void DestroyDoor( bool _explode = true )
    {
        if(_explode == true )
        {
            GameObject.Instantiate(revealEffect, transform.position, Quaternion.identity);
        }

        sao.solid = false;
        GetComponent<Animator>().SetTrigger("breakDoor");
    }
}
