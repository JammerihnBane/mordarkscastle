﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Activator : MonoBehaviour {

    public delegate void ActivatorDelegate( GameObject _activator );
    private ActivatorDelegate activateDelegate = null;
    private ActivatorDelegate deactivateDelegate = null;

    private List<ActivatorDelegate> onActivate = new List<ActivatorDelegate>();
    private List<ActivatorDelegate> onDeactivate = new List<ActivatorDelegate>();

    //This stops the compiler from saying that we don't use these variables.
    private void SUPPRESSSTUPIDWARNINGS()
    {
        activateDelegate( gameObject );
        deactivateDelegate( gameObject );
    }

    public void AddActivator(ActivatorDelegate _activator)
    {
        onActivate.Add(_activator);
    }

    public void AddDeactivator(ActivatorDelegate _activator)
    {
        onDeactivate.Add(_activator);
    }

    public void Activate( GameObject _activator )
    {
        foreach (ActivatorDelegate del in onActivate )
        {
            del(_activator);
        }
    }

    public void Deactivate( GameObject _activator )
    {
        foreach (ActivatorDelegate del in onDeactivate)
        {
            del(_activator);
        }
    }
}
