﻿using UnityEngine;
using System.Collections;

public class SwitchActivator : MonoBehaviour {

    ExtraData exDatRef = null;
    bool active = false;
    bool busy = false;

    // Use this for initialization
    void Start () {
        Activator act = gameObject.GetComponent<Activator>();
        if (act == null)
        {
            Debug.LogError("Button Activator cannot initialise because there is no Activator attached to this object");
        }
        act.AddActivator( Activate );
        act.AddDeactivator( Activate );

        exDatRef = GetComponent<ExtraData>();
    }

    //Activates all of the things connected to this button.
    void Activate(GameObject _obj)
    {
        active = !active;
        if (exDatRef != null && busy == false)
        {
            busy = true;
            foreach (string connection in exDatRef.rawData.connections)
            {
                GameObject obj = WorldManager.worldManager.GetNamedObject(connection, transform.position);
                if (obj != null)
                {
                    Activator act = obj.GetComponent<Activator>();
                    if (act != null)
                    {
                        if (active == true)
                        {
                            Debug.Log("Activating: " + connection);
                            act.Activate(gameObject);
                        }
                        else
                        {
                            Debug.Log("Deactivating: " + connection);
                            act.Deactivate(gameObject);
                        }
                        GetComponent<Animator>().SetBool(Animator.StringToHash("isRight"), !active);
                    }
                }
            }
        }
    }

    void SetBusyFalse()
    {
        busy = false;
    }
}
