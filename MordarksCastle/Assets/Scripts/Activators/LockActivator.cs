﻿using UnityEngine;
using System.Collections;

public class LockActivator : MonoBehaviour {

    public string keyName;
    public GameObject particleEffect;
    public float keyHeldCooldown;
    private SolidActiveObject sao = null;

	// Use this for initialization
	void Start () {
        Activator act = GetComponent<Activator>();
        sao = GetComponent<SolidActiveObject>();
        if (act)
        {
            act.AddActivator( Activate );
        }
        else
        {
            Debug.LogError("Locks must have an activator.");
        }
    }
	
	void Activate(GameObject _obj )
    {
        Player player = _obj.GetComponent<Player>();
        if( player && sao.solid == true )
        {
            if( player.RemoveFromInventory( keyName ) )
            {
                //Mark as non-solid.
                sao.solid = false;
                sao.semiSolid = false;

                GetComponent<Animator>().SetBool("unlocked", true);
            }
        }
    }

    public void Destroy()
    {
        GameObject.Destroy(gameObject);
        Vector3 particlePos = transform.position;
        particlePos.z -= 1;
        GameObject.Instantiate(particleEffect, particlePos, Quaternion.identity);
    }
}
