﻿using UnityEngine;
using System.Collections;

public class TeleporterActivator : MonoBehaviour {

    ExtraData ed = null;
	// Use this for initialization
	void Start () {
        Activator act = GetComponent<Activator>();
        if (act)
        {
            act.AddActivator( Activate );
            ed = GetComponent<ExtraData>();
            if (ed == null)
                Debug.LogError("Teleporters must have an extra data component for their destination.");
        }
        else
        {
            Debug.LogError("Teleporters must have an activator.");
        }
    }

    void Activate(GameObject _activator)
    {
        Player player = _activator.GetComponent<Player>();
        if (player != null)
        {
            if (ed.rawData.connections.Length == 0)
                Debug.LogError("This teleporter doesn't contain a connection. abandoning teleport.");
            else
            {
                //Teleport.
                WorldManager.worldManager.TeleportPlayer(ed.rawData.connections[0]);
            }
        }
    }
}
