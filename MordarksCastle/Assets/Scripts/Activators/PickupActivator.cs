﻿using UnityEngine;
using System.Collections;

public class PickupActivator : MonoBehaviour {

    public string itemName;

    public void Start()
    {
        Activator act = GetComponent<Activator>();
        if( act )
        {
            act.AddActivator( Activate );
        }
        else
        {
            Debug.LogError("Pickup objects must have an activator.");
        }
    }

    public void Activate( GameObject _obj )
    {
        Player player = _obj.GetComponent<Player>();
        if ( player != null )
        {
            player.AddToInventory(itemName);
            GameObject.Destroy(gameObject);
        }
    }

}
