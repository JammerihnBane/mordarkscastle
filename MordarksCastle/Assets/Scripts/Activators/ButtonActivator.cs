﻿using UnityEngine;
using System.Collections;

public class ButtonActivator : MonoBehaviour {

    ExtraData exDatRef = null;
    public bool deactivatable;
    private bool activatedOnce = false;

	// Use this for initialization
	void Start () {
        Activator act = gameObject.GetComponent<Activator>();
        if( act == null )
        {
            Debug.LogError("Button Activator cannot initialise because there is no Activator attached to this object");
        }
        act.AddActivator( Activate );
        act.AddDeactivator( Deactivate );

        exDatRef = GetComponent<ExtraData>();
	}

    //Activates all of the things connected to this button.
    void Activate( GameObject _obj )
    {
        if(deactivatable == true || activatedOnce == false )
        {
            if (exDatRef != null)
            {
                foreach (string connection in exDatRef.rawData.connections)
                {
                    GameObject obj = WorldManager.worldManager.GetNamedObject(connection, transform.position);
                    if (obj != null)
                    {
                        Activator act = obj.GetComponent<Activator>();
                        if (act != null)
                        {
                            Debug.Log("Activating: " + connection);
                            act.Activate(gameObject);

                            activatedOnce = true;

                            Animator anim = GetComponent<Animator>();
                            if (anim)
                                anim.SetBool("pressed", true);
                        }
                    }
                }
            }
        }
    }

    //Dectivates all the things connected to this button.
    void Deactivate( GameObject _obj )
    {
        if (exDatRef != null && deactivatable == true)
        {
            foreach (string connection in exDatRef.rawData.connections)
            {
                GameObject obj = WorldManager.worldManager.GetNamedObject(connection, transform.position);
                if (obj != null)
                {
                    Activator act = obj.GetComponent<Activator>();
                    if (act != null)
                    {
                        Debug.Log("Deactivating: " + connection);
                        act.Deactivate( gameObject );
                    }
                }
            }
        }
    }
}
