﻿using UnityEngine;
using System.Collections;

public class PushActivator : MonoBehaviour {

    private Activator act = null;
    private bool moving = false;
    public float statueMoveTime;

    private SaveData sd = null;

    // Use this for initialization
    void Start () {
        act = GetComponent<Activator>();
        sd = GetComponent<SaveData>();
        if ( act == null )
        {
            Debug.LogError("Cannot have a push activator without an activator");
        }
        else
        {
            act.AddActivator( Activate );
            act.AddDeactivator( Activate );
        }
	}

    public void Activate( GameObject _activator )
    {
        if( moving == false )
        {
            Vector3 posDiff = _activator.transform.position - transform.position;
            posDiff.x = Mathf.RoundToInt(-posDiff.x);
            posDiff.y = Mathf.RoundToInt(-posDiff.y);
            posDiff += transform.position;
            posDiff.z = 0.0f;

            if (WorldManager.worldManager.IsPositionFree(posDiff, true))
            {
                WorldManager.worldManager.DeactivateObjectsAt(transform.position, gameObject);
                iTween.MoveTo(gameObject, iTween.Hash("x", posDiff.x, "y", posDiff.y, "time", statueMoveTime, "oncomplete", "MoveFinished"));
                moving = true;

                SpikeActivator.UnblockSpikesAt(transform.position);
                SpikeActivator.BlockSpikesAt(posDiff);
            }
                
        }
    }

    public void DeActivate( GameObject _activator )
    {

    }

    public void MoveFinished()
    {
        moving = false;
        WorldManager.worldManager.ActivateObjectsAt(transform.position, gameObject);

        if(sd != null )
        {
            sd.rawData.SetPos(transform.position);
            sd.SaveLocally();
            Debug.Log("Updated local save data.");
        }
        else
        {
            Debug.LogWarning("This push activated object has no save data. Its position will not be saved");
        }
    }
}
