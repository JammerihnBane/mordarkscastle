﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    public static GameObject player = null;
    private Dictionary<string, int> inventory = new Dictionary<string, int>();
	
    void Start()
    {
        //Debug.Log("player created");
    }

    //Adds an object to the player's inventory.
    public void AddToInventory( string _obj )
    {
        if(inventory.ContainsKey(_obj))
        {
            ++inventory[_obj];
        }
        else
        {
            inventory[_obj] = 1;
        }
    }

    //Returns the number of a certain object in the player's inventory.
    public int NumberOf( string _obj )
    {
        if (inventory.ContainsKey(_obj))
        {
            return inventory[_obj];
        }
        return 0;
    }

    //Removes one object from the player's inventory.
    //Returns true if the object was removed.
    public bool RemoveFromInventory( string _obj )
    {
        if (inventory.ContainsKey(_obj))
        {
            if(inventory[_obj] > 0)
            {
                --inventory[_obj];
                return true;
            }
            return false;
        }
        return false;
    }
}
