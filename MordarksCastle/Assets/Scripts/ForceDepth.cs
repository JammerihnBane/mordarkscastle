﻿using UnityEngine;
using System.Collections;

public class ForceDepth : MonoBehaviour {

    public float depth;
    public bool addYCoordinate;

	// Use this for initialization
	void Start () {
        DepthUpdate();
	}

    public void DepthUpdate()
    {
        Vector3 pos = transform.position;
        pos.z = depth;
        transform.position = pos;
    }

    void Update()
    {
        if( addYCoordinate )
        {
            DepthUpdate();
            Vector3 pos = transform.position;
            pos.z += pos.y/50;
            transform.position = pos;
        }
    }
        
}
