﻿using UnityEngine;
using System.Collections;

public class HideThis : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<SpriteRenderer>().enabled = false;
	}
}
