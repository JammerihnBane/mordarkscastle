﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

    public float moveSpeed = 0.2f;
    public float autoMoveWaitTime = 0.5f;

    private float autoMoveCounter = 0.0f;
    private Vector3 queuedAction = Vector3.zero;
    private bool moving = false;
    private Room oldRoom = null;

    private Vector3 roomStart = Vector3.zero;

    void Start()
    {
        roomStart = transform.position;
        oldRoom = WorldManager.worldManager.GetRoom(transform.position);
    }
	
	// Update is called once per frame
	void Update () {
        CheckMove();
        CheckMoveHeld();

        if ( Input.GetKeyDown("r"))
        {
            Room currentRoom = WorldManager.worldManager.GetRoom(transform.position);
            if ( currentRoom != null )
            {
                Debug.Log("Restarting current room.");
                currentRoom.RestartRoom();
                transform.position = roomStart;
            }
        }
        if (Input.GetKeyDown("s"))
        {
            SaveGameManager.instance.SaveDataToDisk();
        }
    }

    //If any keys are down, move the player.
    void CheckMove()
    {
        if (Input.GetKeyDown("up"))
            Move(new Vector3(0.0f, 1.0f, 0.0f));
        if (Input.GetKeyDown("down"))
            Move(new Vector3(0.0f, -1.0f, 0.0f));
        if (Input.GetKeyDown("left"))
            Move(new Vector3(-1.0f, 0.0f, 0.0f));
        if (Input.GetKeyDown("right"))
            Move(new Vector3(1.0f, 0.0f, 0.0f));
    }

    void CheckMoveHeld()
    {
        //If nothing is happening, increase the queue move counter.
        if( moving == false && queuedAction == Vector3.zero )
        {
            autoMoveCounter += Time.deltaTime;
        }

        if(autoMoveCounter >= autoMoveWaitTime)
        {
            if (Input.GetKey("up"))
                Move(new Vector3(0.0f, 1.0f, 0.0f));
            else if (Input.GetKey("down"))
                Move(new Vector3(0.0f, -1.0f, 0.0f));
            else if (Input.GetKey("left"))
                Move(new Vector3(-1.0f, 0.0f, 0.0f));
            else if (Input.GetKey("right"))
                Move(new Vector3(1.0f, 0.0f, 0.0f));
            else
            {
                //No movement key is being pressed. Reset wait counter.
                autoMoveCounter = 0.0f;
                queuedAction = Vector3.zero;
            }
        }
    }

    //Move the player around.
    public void Move( Vector3 _direction )
    {
        Vector3 newPos = transform.position + _direction;
        newPos.x = Mathf.Floor(newPos.x) + 0.5f;
        newPos.y = Mathf.Floor(newPos.y) + 0.5f + 0/32;
        if ( moving == true )
        {
            queuedAction = _direction;
        }
        else
        {
            //If the position is free, move the player there.
            if ( WorldManager.worldManager.IsPositionFree( newPos ) )
            {
                WorldManager.worldManager.DeactivateObjectsAt(transform.position, gameObject);
                iTween.MoveTo(gameObject, iTween.Hash("x", newPos.x, "y", newPos.y, "time", moveSpeed, "oncomplete", "MoveFinished"));
                 moving = true;

                SpikeActivator.UnblockSpikesAt(transform.position);
                SpikeActivator.BlockSpikesAt(newPos);
            }

            //Check that the area we are moving to is free.
            List<GameObject> objects = WorldManager.worldManager.GetObjects(newPos);

            //If the object is not null and has an activator, we should activate it.
            foreach (GameObject obj in objects )
            {
                SolidActiveObject solidActiveObject = obj.GetComponent<SolidActiveObject>();
                if (obj != null && solidActiveObject != null && solidActiveObject.solid == true)
                {
                    Activator activator = obj.GetComponent<Activator>();
                    if (activator != null)
                    {
                        activator.Activate(gameObject);
                        autoMoveCounter = 0.0f;
                    }
                }

                
            }
        }
    }

    //We can only move when our move is finished.
    public void MoveFinished()
    {
        //Debug.Log("Move Finished.");
        //Get any objects that might exist underneath us.
        WorldManager.worldManager.ActivateObjectsAt( transform.position, gameObject );

        moving = false;
        if (queuedAction != Vector3.zero)
        {
            Move(queuedAction);
            queuedAction = Vector3.zero;
        }

        //If we change rooms, we need to remember our starting location in that room.
        Room currentRoom = WorldManager.worldManager.GetRoom(transform.position);
        if (currentRoom != oldRoom)
        {
            roomStart = transform.position;
        }
        oldRoom = currentRoom;
    }
}
