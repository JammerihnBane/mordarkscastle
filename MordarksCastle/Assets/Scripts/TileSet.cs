﻿using UnityEngine;
using System.Collections;

public class TileSet : ScriptableObject {
    public Transform[] prefabs = new Transform[0];

    public int ToID(GameObject _obj)
    {
        for (int i = 0; i < prefabs.Length; ++i)
        {
            if (prefabs[i].name == _obj.name)
            {
                return i;
            }
        }
        return -1;
    }
}
