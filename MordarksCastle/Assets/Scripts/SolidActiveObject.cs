﻿using UnityEngine;
using System.Collections;

public class SolidActiveObject : MonoBehaviour {
    public bool solid = true;
    public bool semiSolid = true;
}
