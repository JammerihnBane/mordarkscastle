﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveGameManager {

    public static SaveGameManager instance = null;
    public string saveGameName = "DefaultSave.sav";

    private List<RawSaveData> saveDataBase;

    public static void Create()
    {
        if (instance == null)
        {
            instance = new SaveGameManager();
            instance.saveDataBase = new List<RawSaveData>();
        }
    }

    public void LoadDataFromDisk( string _saveGameName = "" )
    {
        if (_saveGameName != "")
            saveGameName = _saveGameName;

        //We should always have a savegamename.
        BinaryFormatter bf = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + saveGameName;
        if (File.Exists(path))
        {
            FileStream fStream = File.OpenRead(path);
            if (fStream != null)
            {
                saveDataBase = new List<RawSaveData>(); //Get rid of any savedata we might have.
                saveDataBase = (List<RawSaveData>)bf.Deserialize(fStream);
            }
            else
            {
                Debug.LogWarning("Save Data was found but could not be loaded!");
            }
        }
        else
        {
            Debug.LogWarning("Save Data not found: " + path);
        }
    }

    //Returns a specific piece of RawSaveData from the database.
    public RawSaveData LoadDataLocally(string _id)
    {
        foreach( RawSaveData data in saveDataBase)
        {
            //string[] key = data.id.Split('.');
            if (_id == data.id)
            {
                //This is our save data.
                return data;
            }
        }

        return null;
    }

    public void SaveDataLocally(RawSaveData _data)
    {
        if (_data != null) //Never add null data.
        {
            if( _data.id != "")
            {
                bool dataFound = false;
                for (int i = 0; i < saveDataBase.Count; i++)
                {
                    //string[] key = saveDataBase[i].id.Split('.');
                    if (_data.id == saveDataBase[i].id)
                    {
                        //This is our save data. We should overwrite it.
                        saveDataBase[i] = _data;
                        dataFound = true;
                        break;
                    }
                }

                if (dataFound == false)
                    saveDataBase.Add(_data);
            }
            else
            {
                Debug.LogError("The id of this data is BLANK!");
            }
        }
    }

    public void SaveDataToDisk()
    {
        if (saveDataBase != null)
        {
            //Save all the data in our database to a file.
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fStream = File.Create(Application.persistentDataPath + "/" + saveGameName);
            if (fStream != null)
            {
                bf.Serialize(fStream, saveDataBase);
                fStream.Close();
                Debug.Log("Saved the game.");
            }
            else
            {
                Debug.Log("File Stream is null, can't create savegame.");
            }
        }
        else
        {
            Debug.LogError("No save game database exists");
        }
        
    }
}
