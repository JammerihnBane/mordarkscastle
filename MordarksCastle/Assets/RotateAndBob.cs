﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAndBob : MonoBehaviour {

    public Vector3 rotation;
    public float bobAmount;
    public float bobSpeed = 1f;

    private float startY = 0f;
    private float runningClock = 0f;

	// Use this for initialization
	void Start () {
        startY = transform.position.y;
        runningClock += Random.Range(0f, 360f);
        //transform.Rotate(rotation * runningClock*0.01f);
	}
	
	// Update is called once per frame
	void Update () {
        runningClock += Time.deltaTime * bobSpeed;

        transform.Rotate(rotation * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, Mathf.Sin(runningClock) * bobAmount + startY , transform.position.z);
	}
}
